# SockJS Events
This module is under development and should not be used!

## Philosophy
Aim to be as compatible as possible with Socket.IO while using the SockJS library for websockets functionailty.

###Why SockJS? 
SockJS was the websockets library of choice when designing this project, it allows for the use of websockets on devices and servers that don't support it. Plus it's api is simple enought that it can be extented without any major issues.

###Why should I use this extension?
If you need to use websockets and like the api used in Socket.IO, but aren't sure wether your server supports websokets or your clients are stuck using browsers that don't have full support for websockets than you should consider using this extension.

###What versions of node can I use this with?
Any version where SockJS is supported.
