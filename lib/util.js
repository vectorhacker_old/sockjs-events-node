var Stapes = require('stapes'); // we use stapes to provide event handler functionality

var  Events = Stapes.subclass({
  constructor: function() {
    console.log('[creating events]');
  }
});

var events = new Events();

module.exports = exports = events;