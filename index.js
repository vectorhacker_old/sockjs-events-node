// The MIT License (MIT)

// Copyright (c) 2013 Victor A. Martinez Santiago

// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

var sockjs =  require('sockjs');
var events = require('./lib/util');


// Consturctor
function SocksJSEmmiter(prefix) {
  var _self = this;
  this.prefix = prefix || '/io';
  this.echo = sockjs.createServer();

  this.echo.on('connection', function(connection) {
    events.emit('connection', connection);

    connection.on('data', function(data) {
      var e = JSON.parse(data),
          event = e.event,
          data = e.data;
      events.emit('event', {event: event, data: data});
    });
  });
}

SocksJSEmmiter.prototype.socket = {
  on: function(event, callback) {
    events.on('event', function (e) {
      if (event === e.event) {
        callback(e.data); // run callback with an argument of data
      } else {
        console.log('not an event');
      }
    });
  },

  emit: function(event, data) {
    var e
      , message;
    if (typeof event !== 'object') {
      e = event.toString() || JSON.stringify(event);
    } else {
      e = event;
    }

    message = JSON.stringify({
      "event": e,
      "data": data
    });

    this.write(message);
  }
};

SocksJSEmmiter.prototype.listen = function(server) {
  this.echo.installHandlers(server, {prefix: this.prefix});
};

SocksJSEmmiter.prototype.sockets = {
  _self: this,

  on: function(event, callback) {
    switch(event) {
      case 'connection':
        events.on('connection', function (socket) {
          socket.prototype = SocksJSEmmiter.prototype.socket;
          callback(socket);
        });
        break;
      default:
        console.log('please specify an event.');
    }
  }
};



module.exports = exports = SocksJSEmmiter;

